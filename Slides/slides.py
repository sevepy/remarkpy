# Copyright 2020 Severino Tessarin
# Licensed under the MIT license

from jinja2 import Template
from jinja2 import Environment, FileSystemLoader
from .utils import *
from  pathlib import Path
import os
from  shutil import copy


class TempFilesManager:
    """
    Class for temp files
    tf = tuple from mkstemp (file Os descriptor, full path)
    """

    def __init__(self):
        import tempfile
        ##  Create a temporary file
        ##  default file permission rw-r--r--
        ##  in Win cannot set the file permission to x with chmod
        self.tf = tempfile.mkstemp(
            suffix=".html", prefix=None, dir=get_project_root(), text=True
        )

    def remove_tmpfile(self):
        """
        File should be closed from its os file descriptor
        """
        os.close(self._get_osdec())
        os.remove(str(self.tf[1]))

    @staticmethod
    def remove_tmpfile_par(osdesc, filepath):
        """
        close the file with osdec and remove the filepath
        """
        try:
            os.close(osdesc)
        except:
            pass
        os.remove(filepath)

    def _get_osdec(self):
        "Temp file os descriptor"
        return self.tf[0]

    def _ftpath(self):
        "full path/name"
        return self.tf[1]

    def _ftname(self):
        return os.path.basename(self._ftpath())


class Deck(TempFilesManager):
    """
    Deck main class

    : markdown_file_path : markdown file name and extension, the source file
    : isasourceurl  : False reads the and pass the content or True uses source URL
    : themedir      : Theme directory Theme-Name/slideshow.html file
    : notefile      : a file with a .txt extension is designed a notefile type (compatible with wintermnote ) and the first line will be removed from the presentation
    """

    def __init__(
        self, markdown_file_path=None, isasourceurl=True, theme_dir="\\themes\\default",notefile=False
    ):
        ###
        self.markdown_file_path = markdown_file_path
        self.notefile = notefile
        ### Theme directory and theme file
        self.env_dir  = join_path(get_project_root(), theme_dir)
        template_file = join_path(get_project_root(), theme_dir + "\\slideshow.html")

        with open(template_file) as file_:
            self.template_source = file_.read()

        ### initialize base class i.e. create the tmp file
        super().__init__()

        ### Read the markdown source file if passed from the args
        ### https://gitlab.com/Sevepy/remarkpy/-/raw/master/test2.md
        self.path, markdown_file = split_filepath(markdown_file_path)

        if markdown_file is not None and not isasourceurl:
            with open(markdown_file) as mdfile_:
                if notefile:
                    self.source = mdfile_.read()
                else:
                    self.source = "".join(mdfile_.readlines()[1:])
        elif markdown_file.find("http") > -1:
            self.source = markdown_file
        else:
            self.source = "source.tmp"

        self.update_tempfile()

    def Update_Theme(self,theme_dir):

        self.env_dir  = join_path(get_project_root(), theme_dir)
        template_file = join_path(get_project_root(), theme_dir + "\\slideshow.html")

        with open(template_file) as file_:
            self.template_source = file_.read()

    def _get_template_source(self):
        return "{}".format(self.template_source)

    def get_html(self, title: str = "", source: str = "") -> str:
        """Return the html code of the full presentation after has rendered
        the theme template
        :title: HTML title
        :source: Provided Html string
        """
        template = Environment(loader=FileSystemLoader(self.env_dir)).from_string(
            self.template_source
        )
        if source != "":
            return template.render(title=title, source=source)
        else:
            return template.render(title=title, source=self.source)

    def update_tempfile(self):
        with open(self._ftpath(), "w") as f:
            f.write(self.get_html())

    def save_to(self, dest="public"):
        copy(
            self._ftpath(), pathlib.Path(get_project_root(), f"\\{dest}\\index.html")
        )

    def get_tmpfile(self):
        return self._ftname()

    def open_slideshow(self,http_port):
        from . import WebServer

        server = WebServer.WebServer(
            forever=True,
            source = self.markdown_file_path,
            served_file=self._ftname(),
            server_root=get_project_root(),
            tmpfile=self._ftpath(),
            notefile = self.notefile,
            http_port = http_port
        )
        server.run_server(self._get_osdec(), self._ftpath())
