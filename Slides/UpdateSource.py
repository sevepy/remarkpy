from pathlib import Path, PureWindowsPath
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from shutil import copy, SameFileError
from .utils import *
import os,sys,time
## encoding https://stackoverflow.com/questions/9233027/unicodedecodeerror-charmap-codec-cant-decode-byte-x-in-position-y-character#9233174
class MyHandler(FileSystemEventHandler):
    def __init__(self,path,sourcedest, notefile):
        self.path = path
        self.sourcedest = sourcedest
        self.notefile = notefile

    def on_modified(self, event):
        print(f"On modified event type: {event.event_type}  path : {event.src_path}")
        ## exclude source.tmp from on modified event
        if "source.tmp" in event.src_path:
            return
        # restart if event modified session file
        if str(event.src_path).find(".session") > -1:
            # recreate tmp file
            pass
        else:
            try:
                if self.notefile:
                    ## here I am having permission error when I use VIM
                    time.sleep(0.3333)
                    copy(self.path, self.sourcedest)
                    with open(self.sourcedest, "r",encoding='cp437') as f:
                        rows = f.readlines()[1:]
                    with open(self.sourcedest, "w",newline='\n',encoding='cp437') as f:
                        f.write("".join(rows))
                else:
                    time.sleep(0.5)
                    copy(self.path, self.sourcedest)
            except SameFileError:
                print("SameFileError")
                pass

class UpdateSource:
    def __init__(self, path: PureWindowsPath, rootdir: Path, notefile:bool):
        sd = PureWindowsPath(path)
        self.sourcedest = join_path(rootdir, "source.tmp")
        self.path = PureWindowsPath(path)

        if notefile:
            copy(self.path, self.sourcedest)
            with open(self.sourcedest, "r",encoding='cp437') as f:
                rows = f.readlines()[1:]
            with open(self.sourcedest, "w",newline='\n',encoding='cp437') as f:
                f.write("".join(rows))
        else:
            copy(path, self.sourcedest)

        event_handler = MyHandler(path,self.sourcedest,notefile)
        self.observer = Observer()
        self.observer.schedule(event_handler, str(self.path.parent), recursive=False)
        self.observer.start()

    def stop(self):
        os.remove(self.sourcedest)
        self.observer.stop()
        self.observer.join()
