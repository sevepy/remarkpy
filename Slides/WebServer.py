# Copyright 2020 Severino Tessarin
# Licensed under the MIT license
import os
from  pathlib import Path
from  socketserver import TCPServer
from http.server import SimpleHTTPRequestHandler
import functools
from .utils import *
from .slides import TempFilesManager
from Slides import UpdateSource
import time
import pdb
import threading
class NocacheHTTPRequestHandler(SimpleHTTPRequestHandler):
    def do_POST(self):
        def close_server(server):
            server.shutdown()
        # threading.Thread(target = close_server,args = (httpd,))
        self.send_error(500)
    def end_headers(self):
        self.set_nocache_headers()
        SimpleHTTPRequestHandler.end_headers(self)

    def set_nocache_headers(self):
        self.send_header(
            "Access-Control-Allow-Origin: *",
            "no-store, no-cache, must-revalidate, post-check=0, pre-check=0",
        )
        self.send_header("Pragma", "no-cache")
        self.send_header("Expires", "0")


class Browser:
    def __init__(self, Url):
        self.browser = get_browser() 
        self.Url = Url
    
    async def runbrowser(self):
        # similar to https://github.com/jarun/ddgr/blob/master/ddgr
        #from subprocess import Popen
        self.browser.open(self.Url,new = 2)

    def openpage(self):
        "open html page in the browser"
        import asyncio
        asyncio.run(self.runbrowser())

class WebServer(TempFilesManager):
    
    def __init__(
        self,
        forever: bool = True,
        source = None,
        server_root=get_project_root(),
        served_file: str = "Slides\\test\\test_output.html",
        tmpfile: Path = None,
        notefile = False,
        http_port = "8065"
    ):
        """
    Class constructor, it can create its own tmpfile or get the path of one which already exists

    : param forever     : server shutdown from keyboard if True, close gracefully after seconds if false
    : param source      : The file name of the sourceUrl file
    : param server_root : The module root directory 
    : param served_file : Default value test value else the tmp file
    : param tmpfile     : The full path of the tempfile (used in the test only)
        """

        self.notefile=notefile

        if not os.path.isfile(served_file):
            super().__init__()
            self.tmpfile = self._ftpath()
        else:
            self.tmpfile = served_file
        self.allow_reuse_address = True
        self.forever = forever
        self.source = source
        self.PORT = int(http_port)
        self.Url = f"http://localhost:{self.PORT}/{served_file}"
        self.Handler = functools.partial(
            NocacheHTTPRequestHandler, directory=server_root
        )
        

    def _get_tmpfile_path(self):
        return self.tmpfile

    def run_server(self, osdesc, filepath):
        with TCPServer(("", self.PORT), self.Handler) as httpd:
            if self.forever:
                s = UpdateSource.UpdateSource(
                    path=self.source, rootdir=get_project_root(), notefile=self.notefile
                )
                browser = Browser(self.Url)
                browser.openpage()
                httpd.serve_forever()
            else:
                test_file = pathlib.Path(
                    get_project_root(), "Slides\\test\\test_output.html"
                )
                browser = Browser(test_file)
                browser.openpage(browser)
            s.stop()
            self.remove_tmpfile()
            httpd.shutdown()
            httpd.server_close()
