layout: true
name: layout
background-color: yellow
background-image: url()


---

# HTML5 Presentations 

## Screencast 2 - Gitlab CI/CD

### Author Name

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

---

## Outline

Generate an HTML5 slideshow from a markdown file on Gitlab/Pages.
 
- PC running Windows 10 connected to internet.

- A Gitlab account (www.gitlab.com).
 
- Git client - Git for Windows (https://git-scm.com/download/win).

<a href="http://tessarinseve.pythonanywhere.com" class="m-link-wrap">
HOME</a>
<p class="m-text-center m-text m-dim">
  There is a <a href="#" class="m-flat">extra</a> link.
</p>
---

## New slide

### 5/24/2020 5:56 PM

$$ f(x) =\frac{k_2}{x^4} $$


```python
def add(a,b):
  return a + b

```
---

## Components m.css
<div class="m-block m-badge m-success">
  <img src="https://sevepy.gitlab.io/genericstaticwebsite/img/my.png" alt="The Author" />
  <h3>About the author</h3>
  <p><a href="#">The Author</a></p>
</div>
<aside class="m-block m-default">
  <h3>Default block</h3>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ultrices
  a erat eu suscipit. <a href="#">Link.</a>
</aside>

---


