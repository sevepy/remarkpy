import os
from Slides import *
def test_jinja_base():
    import jinja2
    try:
        with open(os.getcwd()+"\\Slides\\html\\test.html") as f:
            tem=jinja2.Template(f.read())
        print("{}".format(tem.render(test="OK")))
    except FileNotFoundError:
        print("Test run not in the root dir")

def test_deck_template():
    from Slides import Deck
    deck=Deck(markdown_file_path="\\Slides\\test\\test.md")
    with open(os.getcwd()+"\\Slides\\test\\test_output.html","w") as f:
        f.write("{}".format(deck.get_html("test title","## Deck and webserver test passed")))
    deck.remove_tmpfile()

def test_deck_mdfile():
    from Slides import Deck
    deck=Deck(markdown_file_path="\\Slides\\test\\test.md")
    deck.remove_tmpfile()

def test_openbrowser():
    from Slides import  utils
    a=utils.get_browser()
    a.open("https://tessarinseve.pythonanywhere.com")

def test_create_delete_tmpfile():
     from Slides import WebServer
     server=WebServer.WebServer(forever=False,served_file = "",tmpfile = "")
     print("tmp file location: {}".format(server._get_tmpfile_path()))
     server.remove_tmpfile()

def test_joinpath():
    print(utils.join_path(utils.get_project_root(),"\\Slides\\test\\test.md"))

def test_update_source():
    from Slides import UpdateSource
    print("Looking for test file in root dir {}".format(utils.get_project_root()))
    s=UpdateSource.UpdateSource(path = utils.join_path(utils.get_project_root(),"\\Slides\\test\\test.md"),
            rootdir=get_project_root(), notefile=False)
    assert os.path.isfile(utils.join_path(get_project_root(),"\\source.tmp")) == True ,"Check if tmp copy of the source exists "
    s.stop()
