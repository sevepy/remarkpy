from pathlib import PureWindowsPath,Path
import platform
import os
from shutil import copyfile,copytree
from typing import Union
import webbrowser
import mimetypes
import base64
def createsymlink(static_dir_path:Path):
    """User can create a link to static folder, has to run as Administrator
    """
    dirname = os.path.dirname(static_dir_path)
    target = PureWindowsPath(get_project_root(),dirname)
    os.system(f'mklink {static_dir_path} {target}')


def get_list_of_static_files() ->list:
    """
    Return
    ======

    Return a list of the server static files
    """

    static_files=[]
    for file in os.listdir(join_path(get_project_root(),"\\static\\files\\")):
        if not file.startswith("."):
            static_files.append(file)
    return static_files

def savestaticfolder(static_dir_path:Path):
    """
    copy am entire folder to the root dir
    """
    dirname = os.path.basename(static_dir_path)
    if dirname in ["static","Slides","theme","iconified","background"]:
        raise OSError
    target  = PureWindowsPath(get_project_root(),dirname)
    print(f"{static_dir_path} {target}")

def savestatic(static_file_path:Path):
    """
    copy a file to the local static/files
    """
    file = split_filepath(static_file_path)
    dest = join_path(get_project_root(),f"\\static\\files\\{file[1]}")
    try:
        copyfile(static_file_path,dest)
    except:
        pass

def join_path(root:Path,ext:Union[Path,str])->PureWindowsPath:
    if ext.startswith("\\"):
        s=ext.split("\\")
        ext= "\\".join(s[1:])
    return PureWindowsPath(root, ext)

def get_project_root() -> Path:
    return Path(__file__).parent.parent

def get_currentdir()->Union[Path,str]:
    return os.getcwd()

def split_filepath(fullfilename)->tuple:
    """
    Return
    ======
    (path,filename)
    """

    try:
        return os.path.split(os.path.abspath(fullfilename))
    except TypeError:
        return (get_project_root(),fullfilename )

def get_os_type()-> str:
    """
    Linux: Linux
    Mac: Darwin
    Windows: Windows
    """
    return platform.system()

def get_browser()->webbrowser:
    """
    Return a webbrowser instance for the default webbrowser
    """

    try:
        return webbrowser.get()
    except :
        raise OSError

def img_to_data(path)->str:
    """Convert a file (specified by a path) into a data URI."""
    if not os.path.exists(path):
        raise FileNotFoundError
    mime, _ = mimetypes.guess_type(path)
    with open(path, 'rb') as fp:
        data = fp.read()
        data64 = (base64.b64encode(data))
        figure = os.path.basename(path)
        return '![{0}](data:{1};base64,{2})'.format(figure, mime, data64.decode("utf-8"))

