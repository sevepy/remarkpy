from dataclasses import dataclass
from typing import List


@dataclass
class baseSlide:
    """
    Base class for a  single slide
    """

    name: str
    slide_class: List
    # = field(default_factory=lambda : ["left", "center", "right", "top", "middle", "bottom"])
    background_image: str = ""  ## url
    count: bool = True
    template: str = ""
    layout: bool = False  ## layout slide
    exclude: bool = False  ##filter the slide out


class slide(baseSlide):
    def __init__(self, name, content=""):
        self.name = name
        self.content = content

    def __repr__(self):
        slide = """name:{0}
{1}""".format(
            self.name, self.content
        )
        return slide



