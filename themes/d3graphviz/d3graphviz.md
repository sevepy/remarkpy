layout: true
class: middle

---
## D3Graphviz Remarkpy Theme

### @seve_py


---
class: middle,center

## VIM Modes

.d3graphviz[
digraph VIMmodes {
    size ="6,3";
    ratio=fill;
    nodesep=0.5;
    rankdir=LR;  //Rank Direction Left to Right
    bgcolor=lightgray;
    node [shape=plaintext, fontsize=16];
    "File Status" [shape=ellipse,color=blue, pos="0,0!"]; 
    "Normal Mode" [shape=box,fillcolor = azure,style=filled,pos="3,3!"]; 
    "Command Mode" [shape=box,width=2,fillcolor = yellow,style=filled,pos="4,3!"]; 
    "Insert Mode"  [shape=box, fillcolor = yellow,width=2,style=filled,pos="5,3!"];
    "Visual Mode"  [shape=box, fillcolor = yellow,width=2,style=filled,pos="6,3!"];
    "Normal Mode" -> "Insert Mode" [label=" i  a  c",style=bold,color=blue];
    "Normal Mode":s -> "Visual Mode" [style=bold,label=" v  V  CTRL-v ",labeldistance=4.5,labelfloat=false];
    "Normal Mode":n -> "Command Mode" [style=bold,label=" :  /  ?"];
    "Command Mode" -> "Normal Mode" [style=bold,label=" ESC     command CR"]; 
    "Insert Mode" -> "Normal Mode" [style=bold,label=" ESC    "];   
    "Normal Mode"  -> "File Status" [style=bold,label=" CTRL-g     "];
}



]

---

## Graph 2

.d3graphviz[
    digraph  {a -> b}
]







