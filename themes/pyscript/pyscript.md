layout: true
class: middle

---
## PyScript Remarkpy Theme

### @seve_py

---
##


---
class: middle,center

## PyScript

<div id="graph"></div>
<py-script output="graph">
from matplotlib import pyplot as plt
import numpy as np
import math as m
fig=plt.figure()
x=np.arange(10)
y=(lambda x:1+np.sqrt(x))(x)
ax=plt.gca()
ax.set_title("simple plot")
plt.plot(x,y,'bo-')
fig
</py-script>






