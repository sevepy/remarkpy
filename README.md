## Remarkpy


This repository provides a graphical interface for Remark Markdown-HTML presentation tool. 

Download the self-extracting installer from [buymeacoffe.com](https://www.buymeacoffee.com/seve/remarkpy-windows-markdown-presentation-tool) and then double click on the desktop shortcut. From the GUI select your Remark presentation text file (e.g. example.md in the repo). Your default browser then will open a new tab to display your presentation. The markdown file and the presentation will remain linked, thus if you edit the markdown file with notepad and refresh the browser, it will show the latest version.

Read the following [technical note](https://www.buymeacoffee.com/seve/remarkpy-windows-markdown-presentation-tool) for a more detailed explanation. 






