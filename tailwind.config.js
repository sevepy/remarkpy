module.exports = {
  content: ["./*.html"],
  darkMode: 'class',
  theme: {
                       extend: {
                                fontFamily: {
                                         'Cambria': ['Cambria', 'serif'] 
                                     },
                                animation: {
                                         wiggle: 'wiggle 1s ease-in-out infinite',
                                     },
                                colors: {
                                         clifford: '#da373d',
                                     }
                            }
  },
  plugins: [require('@tailwindcss/typography'),
  ],
}
