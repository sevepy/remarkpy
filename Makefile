.PHONY: test,clean, force_rm, public, test-mypy, tags, tailwind
test-mypy:
	mypy remarkpy.py
test:
	python -m pytest -xs 

force_rm:
	rm tmp*.html

tailwind:
	npx tailwindcss -i ./tw.css -o ./static/css/styletw.css 

clean:
	- make force_rm
	- rm -rf __pycache__
	- rm *.spec
	- rm -rf dist
	- rm -rf build
	- rm -rf ./output
	- rm static/files/*.jpg
	- rm static/files/*.svg
	- rm static/files/*.png
	- rm remarkpy_settings.txt
	- rm source.tmp

build:
	./compile.sh

tags:
	ctags --recurse=yes --exclude=.git --exclude=build --exclude=dist --exclude=node_modules/* 
