#!/usr/bin/env python
# Copyright 2020-2023  Severino Tessarin
# Licensed under the MIT license
import os
from easygui import (
    choicebox,
    msgbox,
    filesavebox,
    fileopenbox,
    diropenbox,
    buttonbox,
    enterbox,
    exceptionbox,
    EgStore,
)
import sys
from Slides import Deck, utils, WebServer
from pathlib import PureWindowsPath, Path
from subprocess import call
import pyperclip
import threading
import glob
# import pdb
from PIL import ImageGrab
from shutil import copy

__version__ = "0.3.2"


class FileTypeError(Exception):
    pass

class Settings(EgStore):
    def __init__(self, filename):  # filename is required   # specify default values for variables that this application wants to remember
        self.theme_dir = '\\themes\\default'
        self.text_editor = 'notepad.exe'
        self.http_port = '8065'
        self.clipname = 'Image'
        self.presentation_dir = Path(Path.home(),"*")
        self.initialize = True
        self.filename = filename

def open_new_url(url):
    opn = WebServer.Browser(url)
    # print("Opening...{}", url)
    opn.openpage()


def open_texteditor(presentation, text_editor):
    print(f"open {presentation} with {text_editor} ...")
    ## build the command
    cmd = []
    if os.path.exists(text_editor):
        cmd.append(text_editor)
    else:
        for c in text_editor.split(" "):
            cmd.append(c)
    cmd.append(str(presentation))
    # print(f"{cmd}")
    call(cmd)
# The only time you need to specify shell=True on Windows is when the command you wish to execute is built into the shell (e.g. dir or copy). You do not need shell=True to run a batch file or console-based executable

if __name__ == "__main__":

    # check if debug
    if len(sys.argv) < 2:
        os.chdir("./_internal/")

    theme = "default"
    http_port = "8065"
    openslidedeck = False
    threads = []
    working_presentation = []
    settings = Settings('remarkpy_settings.txt')
    settings.restore()

    if settings.initialize:
        settings.initialize = False
        settings.store()
    else:
        pass

    theme_dir = settings.theme_dir
    theme = theme_dir.replace("\\themes\\", "")
    text_editor = settings.text_editor
    http_port = settings.http_port
    clipname = settings.clipname
    presentation_dir = settings.presentation_dir

    title = f"Remark Markdown Presentation Tool  v. {__version__}"
    choices = [
        "Select Theme",
        "Server HTTP Port",
        "Select Text Editor",
        "Open Presentation",
        "Edit Presentation",
        "Add Figure To Clipboard",
        "Add static file",
        "Save clipboard as static image",
        "Listing static files",
        "Open static directory",
        "Create Empty Presentation",
        "Donate",
        "Help",
        "Restart",
        "Close",
    ]
    while 1:
        msg = f"Working Configuration:\nTheme: {theme}\nPresentation: {working_presentation}\nText Editor: {text_editor}\nHTTP port: {http_port}\n\nSelect option:"
        choice = choicebox(msg, title, choices)
        if choice == "Close" or choice is None:
            # clean up tmp files
            # Get a list of all the file paths that start with tmp* from in specified directory
            filelist = glob.glob(str(utils.get_project_root()) + "\\tmp*")
            # iterate over the list of filepaths & remove each file.
            for filepath in filelist:
                try:
                    os.remove(filepath)
                except:
                    pass
            sys.exit(0)
        elif choice == "Select Theme":
            # themes are based on subfolder  with base.html slideshow.html couple of files
            availables_themes = tuple(
                os.listdir(PureWindowsPath(utils.get_project_root(), "themes"))
            )

            theme = buttonbox("Select Theme", "Available themes", availables_themes)
            if theme is not None:
                settings.theme_dir = "\\themes\\" + theme
                settings.store()
                if working_presentation:
                    deck.Update_Theme(settings.theme_dir)
                    deck.update_tempfile()
            openslidedeck = False
        elif choice == "Server HTTP Port":
            http_port_updated = enterbox(
                "Enter new HTTP Port", "Server HTTP Port", http_port
            )
            if http_port_updated is not None and http_port_updated != http_port:
                http_port = http_port_updated
                settings.http_port = http_port_updated
                settings.store()
                os.execl(sys.executable, os.path.abspath(__file__), *sys.argv)
        elif choice == "Select Text Editor":
            text_editor_updated = enterbox(
                "Enter new editor executable", "Select text editor", text_editor
            )
            if text_editor_updated is not None:
                text_editor = text_editor_updated
                settings.text_editor = text_editor_updated
                settings.store()
            openslidedeck = False
        elif choice == "Open Presentation":
            if working_presentation:
                url = f"http://localhost:{http_port}/" + deck.get_tmpfile()
                thread_openurl3 = threading.Thread(target=open_new_url, args=(url,))
                threads.append(thread_openurl3)
                thread_openurl3.daemon = True
                thread_openurl3.start()
                openslidedeck = False
            else:
                try:
                    msg = "Please select a markdown presentation/note"
                    fullfilename = fileopenbox(
                        msg,
                        title="Select .md, .txt or .wiki /remark file",
                        default = presentation_dir,
                        filetypes=["*.md", "*.html.wiki", "*.txt"],
                    )
                    if PureWindowsPath(fullfilename).suffix not in [
                        ".md",
                        ".wiki",
                        ".txt",
                    ]:
                        raise FileTypeError
                    fn = Path(fullfilename)
                    if fn.suffix == ".txt":
                        notefile = True
                    else:
                        notefile = False
                    presentation_dir = Path(fn.parent,"*")
                    settings.presentation_dir = presentation_dir
                    settings.store()
                    # print(fn.parent)
                    openslidedeck = True
                    # print(f"loading...{fullfilename}")
                    working_presentation.append(fullfilename)
                except (FileTypeError, TypeError):
                    msgbox(msg="Select a .txt .md/.html.wiki file")
                    openslidedeck = False
                if openslidedeck:
                    def open_slideshow(deck,http_port):
                        print("starting a new thread...")
                        try:
                            deck.open_slideshow(http_port)
                        except OSError:
                            exceptionbox("You have another instance running")
                            sys.exit(1)

                    # Remove tmp source if exits
                    source = str(utils.get_project_root()) + "\\source.tmp"
                    try:
                        os.remove(filepath)
                    except:
                        pass
                    deck = Deck(
                        markdown_file_path=fullfilename,
                        theme_dir = settings.theme_dir,
                        notefile = notefile,
                    )
                    thread_presentation = threading.Thread(
                        target=open_slideshow, args=(deck,http_port,)
                    )
                    threads.append(thread_presentation)
                    thread_presentation.daemon = True  # This thread dies when main thread (only non-daemon thread) exits.

                    thread_presentation.start()
                    # pdb.set_trace()
        elif choice == "Edit Presentation":
            if working_presentation:
                thread_texteditor = threading.Thread(
                    target=open_texteditor, args=(working_presentation[0], text_editor,)
                )
                threads.append(thread_texteditor)
                thread_texteditor.daemon = True
                thread_texteditor.start()
            else:
                msgbox("Open a presentation first.")

        elif choice == "Add Figure To Clipboard":
            try:
                msg = "Select a PNG file"
                fullfilename = fileopenbox(
                    msg, title="Select .png File",
                    default = presentation_dir,
                    filetypes=["*.png"]
                )
                if PureWindowsPath(fullfilename).suffix not in [".png"]:
                    raise FileTypeError
                pyperclip.copy(utils.img_to_data(fullfilename))
                msgbox(msg=f"{fullfilename} saved to the clipboard.")
            except (FileTypeError, TypeError):
                msgbox(msg="Select a .png File")

        elif choice == "Add static file":
            msg = """
            Append static file - Relative Url /static/files/filename \n
            Usage : background-image: url(/static/files/filename)
            """
            staticfile = fileopenbox(msg, title="Select file", default = presentation_dir)
            utils.savestatic(static_file_path=staticfile)
            try:
                pyperclip.copy("/static/files/"+os.path.basename(staticfile))
            except:
                pass
            msg = "Static Files:\n"
            msg = msg + "\n".join(utils.get_list_of_static_files())

        elif choice == "Save clipboard as static image":
            img = ImageGrab.grabclipboard()
            if settings.clipname != "":
                clipname = settings.clipname
            else:
                clipname = "image"
            clipname_entered = enterbox(
                f"![{clipname}](static/files/{clipname}.png)", "Image filename", clipname
            )

            try:
                img.save(str(utils.get_project_root())+f"\\static\\files\\{clipname_entered}.png","PNG")
                pyperclip.copy(f"![{clipname}](static/files/{clipname_entered}.png)")
                settings.clipname = clipname_entered
                settings.store()
                clipname = clipname_entered
                msgbox(msg="Clipboard Saved!")
            except:
                msgbox(msg="Not an Image")
        elif choice == "Listing static files":
            print("Listing static files")
            if working_presentation:
                url = f"http://localhost:{http_port}/static/files"
                thread_openurl2 = threading.Thread(target=open_new_url, args=(url,))
                threads.append(thread_openurl2)
                thread_openurl2.daemon = True
                thread_openurl2.start()
                openslidedeck = False

        elif choice == "Open static directory":
            cmd =["explorer.exe"]
            cmd.append(str(utils.get_project_root())+"\\static\\files\\")
            call(cmd, shell=False)

        elif choice == "Create Empty Presentation":
            msg = "Please select the destination file"
            fullfilename = filesavebox(
                msg,
                title="Select filename",
                filetypes=["*.md", "*.html.wiki", "*.txt"],
                default=theme + ".md",
            )
            try:
                os.chdir(presentation_dir)
            except:
                pass
            theme_empty_presentation = settings.theme_dir + "\\" + theme + ".md"
            src = str(utils.get_project_root()) + "\\" + theme_empty_presentation
            # pdb.set_trace()
            if fullfilename is not None and fullfilename != theme_dir + ".md":
                copy(src, fullfilename)
            openslidedeck = False

        elif choice == "Add static folder":
            msg = "Select static folder "
            staticfolder = diropenbox(msg, title="Select static folder")
            utils.savestaticfolder(staticfolder)
            msg = "Static Folder Created\n"

        elif choice == "Donate":
            url = "https://www.buymeacoffee.com/seve"
            thread_openurl = threading.Thread(target=open_new_url, args=(url,))
            threads.append(thread_openurl)
            thread_openurl.daemon = True
            thread_openurl.start()
            openslidedeck = False

        elif choice == "Restart":
            settings.presentation_dir = presentation_dir
            settings.store()
            os.execl(sys.executable, os.path.abspath(__file__), *sys.argv)


        elif choice == "Help":
            help = f"""

            Remarkpy v. {__version__}

            Valid Text Editor:
                notepad.exe (default)
                gvim.exe [2]
                wt.exe -M vim [1][2]
                code.exe [2]

            [1] : Windows Terminal required.

            [2] : Full path maybe required.

            To edit/create a new presentation, you need to close and
            start the program again.

            Blog:
            https://tessarinseve.pythonanywhere.com/nws/tag/remarkpy.html

            Copyright 2020-2023, S. Tessarin

            """
            msgbox(msg=help)
