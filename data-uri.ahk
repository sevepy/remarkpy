#NoEnv
#Warn
#SingleInstance, Force
SetWorkingDir %A_ScriptDir%
OnError("Exit_with_grace")
Exit_with_grace(exception){
return
}
!^q::
MsgBox, Closing the application....
ExitApp

!^f::
FileSelectFile, SelectedFile, 3, , Open an image file (*.png; *.jpg)
If ! FileExist( SelectedFile )
Return
;URLDownloadToFile, http://i.imgur.com/dS56Ewu.png, %SelectedFile%

FileGetSize, nBytes, %SelectedFile%
FileRead, Bin, *c %SelectedFile%
SplitPath, SelectedFile, filename
mime := GetMimeType(SelectedFile)
base := Base64Enc( Bin, nBytes, 100, 2 ) 
B64Data := "![" . filename . "](" . mime . base . ")"
Clipboard := B64Data 
MsgBox, %filename% saved to the clipboard
Return ;     // end of auto-execute section //


GetMimeType(file) {
	default := "text/plain"
	SplitPath, file,,, ext
	switch ext
{	
	case "svg":
	type := "data:image/svg+xml;base64,"
	case "png":
	type := "data:image/png;base64,"
	default :
	throw Exception("Mime not found", -1)

}

	if (!type)
		return default
	return type
}


Base64Enc( ByRef Bin, nBytes, LineLength := 64, LeadingSpaces := 0 ) { ; By SKAN / 18-Aug-2017
Local Rqd := 0, B64, B := "", N := 0 - LineLength + 1  ; CRYPT_STRING_BASE64 := 0x1
  DllCall( "Crypt32.dll\CryptBinaryToString", "Ptr",&Bin ,"UInt",nBytes, "UInt",0x1, "Ptr",0,   "UIntP",Rqd )
  VarSetCapacity( B64, Rqd * ( A_Isunicode ? 2 : 1 ), 0 )
  DllCall( "Crypt32.dll\CryptBinaryToString", "Ptr",&Bin, "UInt",nBytes, "UInt",0x1, "Str",B64, "UIntP",Rqd )
  If ( LineLength = 64 and ! LeadingSpaces )
    Return B64
  B64 := StrReplace( B64, "`r`n" )        
  Loop % Ceil( StrLen(B64) / LineLength )
    B .= Format("{1:" LeadingSpaces "s}","" ) . SubStr( B64, N += LineLength, LineLength ) . "`n" 
Return RTrim( B,"`n" )    
}
